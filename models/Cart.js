const mongoose = require('mongoose');
//const Schema = mongoose.Schema;

// Creating Schema
let CartSchema = new mongoose.Schema({
    //Recipe dbName
    name: {
        type: String,
        required: true
    },
    // Count of People, relevant for Shopping List -> Divisor of Ingredient Counts
    items:[{
        name: String,
        count: Number
    }],
    userID: {
        type: String,
        required: true
    }
}, { versionKey: false });

mongoose.model('cart', CartSchema);