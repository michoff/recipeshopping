const mongoose = require('mongoose');
// Creating Schema
const ingredientSchema = new mongoose.Schema(
    {
        ingName: String,
        ingCount: Number,
        ingCountMetric: String
    });

const RecipeSchema = new mongoose.Schema({
    id: {
        type: Number,
        autoIncrement: true
    },
    // Name of the recipe
    name: {
        type: String,
        required: true
    },
    // Count of People, relevant for Shopping List -> Divisor of Ingredient Counts
    persons: {
        type: Number,
        default: 2,
        required: true
    },
    //Array of Ingredients with a dbName and count
    ingredients: [ingredientSchema],
    // Personal Notes, not Shared when shared
    personalNotes: String,
    instructions: String,
    dateCreated: {
        type: Date,
        default: Date.now()
    },
    // Creater of the recipe
    username: String
}, {versionKey: false});

mongoose.model('Recipe', RecipeSchema);