const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

let UserSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true
    },
    password: String
}, { versionKey: false });

UserSchema.plugin(passportLocalMongoose);  // extends User model with passport methods

mongoose.model('User', UserSchema);
