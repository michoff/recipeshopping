const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
// Load Recipe Model
require('../models/Recipe');
const Recipe = mongoose.model('Recipe');

// Add Recipes Page
router.get('/add', isLoggedIn, (req, res) => {
    res.render('recipes/add', {
        title: 'Add a Recipe',
    });
});
// Edit Recipes Page
router.get('/edit/:id', isLoggedIn, (req, res) => {
    Recipe.findOne({
        _id: req.params.id
    })
        .then(recipe => {
            res.render('recipes/edit', {
                title: 'Edit your recipe',
                recipe: recipe
            });
        })

});

router.get('/show', isLoggedIn, (req, res) => {

    Recipe.find({username: req.user.username})
        .sort({date: 'desc'})
        .then(recipes => {
            res.render('recipes/show', {
                recipes: recipes,
                title: 'Show Recipes'
            })
        })

});


// Process Add Recipes
router.post('/', isLoggedIn, (req, res) => {
    console.log('im in add recipe post, username: ' + req.body.username);
    const newRecipe = {
        persons: req.body.persons,
        name: req.body.name,
        instructions: req.body.instructions,
        personalNotes: req.body.personalNotes,
        ingredients: req.body.ingredients,
        username: req.user.username
    };
    new Recipe(newRecipe)
        .save()
        .then(recipe => {
            res.redirect('/recipes/show')
        })  // .then
        .catch(err => {
            console.log('Recipe save  ' + err)
        });  // .catch of save
});

router.put('/:id', isLoggedIn, (req, res) => {
    Recipe.findOne({
        _id: req.params.id
    })  //Recipe.findeOne
        .then(recipe => {
            //new values
            recipe.persons = req.body.persons;
            recipe.name = req.body.name;
            recipe.instructions = req.body.instructions;
            recipe.personalNotes = req.body.personalNotes;
            recipe.ingredients = req.body.ingredients;

            recipe.save()
                .then(recipe => { // updated recipe
                    res.redirect('/recipes/show')
                })  //.then of save
                .catch(err => {
                    console.log("1" + err)
                })  // catch of findOne
        })  // .then of findOne
        .catch(err => {
            console.log("2" + err)
        })  // catch of findOne
});

router.delete('/show/:id', isLoggedIn, (req, res) => {
    console.log('Here we are in DELETE');
    Recipe.remove({
        _id: req.params.id
    })  // remove
        .then(() => {
            res.redirect('/recipes/show')
        })  // .then of remove
        .catch(err => {
            console.log('Delete Recipe Error:  ' + err)
        })  // .catch of remove
}); // router.delete

// helper middleware to check  if user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    res.redirect('/user/login')
}

module.exports = router;