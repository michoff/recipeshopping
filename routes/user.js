const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const passportLocalMongoose = require('passport-local-mongoose');

// Load User Model
require('../models/User');
const User = mongoose.model('User');

// Login ROUTEs
// render login forms
router.get('/login', (req, res) => {
    if (req.user) { // if already logged in
        res.redirect('/user/profile');
    } else {
        res.render('user/login');
    }
});

// login logic
router.post('/login', passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/user/login",
    failureFlash: true
}), (req, res) => {

});

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

router.get('/register', (req, res) => {
    if (!req.user) {  // not logged in
        res.render('user/register', {
            title: 'Register'
        });
    } else { // logged in, redirect to profile
        res.redirect('/')
    }
});

router.post('/', (req, res) => {
    console.log('im in POST of registration');
    User.register({username: req.body.username}, req.body.password, (err, user) => {
        if (err) {
            console.log(err);
            return res.render('user/register', {user: User.username})
        }
        passport.authenticate('local')(req, res, () => {
            console.log('i should be registrated');
            res.redirect('/');
        });
    });
});

module.exports = router;