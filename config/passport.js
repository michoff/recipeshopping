const LocalStrategy = require('passport-local')
    .Strategy;
const mongoose = require('mongoose');
const User = mongoose.model('User');



module.exports = function(passport) {
    passport.use(new LocalStrategy(User.authenticate()));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());


};