const dbName="recipeDatabase";  // Name of the indexedDB

// checks if db exists already, if not. creates one
databaseExists(dbName,function(exists){
    if(!exists) {
        window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction
            || {READ_WRITE: "readwrite"}; // This line should only be needed if it is needed to support the object's constants for older browsers
        window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

        if (!window.indexedDB) {
            window.alert("Your browser doesn't support a stable version of IndexedDB. " +
                "Such and such feature will not be available.");
        } else {
            console.log("IndexedDB successfully created")
        }
    }
    openIndexedDB(dbName)

});

function openIndexedDB(dbName) {
    let request = window.indexedDB.open(dbName, 3);

    request.onerror = function(event) {
        console.log("IndexedDB couldn't be created")
    };
    request.onsuccess = function(event) {
        console.log("Successfully conntected to indexedDB");
        syncDB(dbName);
        window.addEventListener('online', OnlineChange);
        window.addEventListener('offline', OfflineChange);
    };
}
// adds EventListener for online / offline sync features



/**
 * Informs the user of his offline status
 */
function OfflineChange() {
    window.alert("You are offline. Your changes will be synchronized once your are back online.")
}

// checks if a db is created, if so, starts sync loop
function OnlineChange() {
    window.alert("your are online");
    syncDB(dbName)
}

/**
 * synchronization loop for given local Database with the server-side db every 5 seconds
 * @param name dbName of the database
 */
function syncDB(name) {
    /*let intervalId = setInterval(function() {
        databaseExists(name,function(exists){
            if(exists && navigator.onLine) {
                transferDataToDb()
            } else {
                stopSync(intervalId);  // when offline, stop sync loop
            }
        });
    }, 500);  // 5 seconds

     */
}

// transfers indexedDB content to server-side database
function transferDataToDb() {
    console.log("Transfer Stuff here. Online btw.")
}

// stops the sync loop
function stopSync(stopId) {
    window.clearInterval(stopId)
}

/**
 * Check if a database exists
 * @param {string} name Database dbName
 * @param {function} callback Function to return the response
 * @returns {bool} True if the database exists
 */
function databaseExists(name,callback){
    let dbExists = true;
    let request = window.indexedDB.open(name);
    request.onupgradeneeded = function (e){
        if(request.result.version===1){
            dbExists = false;
            window.indexedDB.deleteDatabase(name);
            if(callback)
                callback(dbExists);
        }

    };
    request.onsuccess = function(e) {
        if(dbExists){
            if(callback)
                callback(dbExists);
        }
    };
}