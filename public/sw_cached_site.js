importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

console.log('Hello from me, the sw!');
if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`);
} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

const bgSyncPlugin = new workbox.backgroundSync.Plugin('requestPostQueue', {
    maxRetentionTime: 24 * 60 // Retry for max of 24 Hours
});

// getting delete and put
workbox.routing.registerRoute(
    new RegExp('.*(method=(DELETE|PUT))'),
    new workbox.strategies.NetworkOnly({
        plugins: [bgSyncPlugin]
    }),
    'POST'
);

// getting delete and put
workbox.routing.registerRoute(
    new RegExp('.*\\/recipes.*'),
    new workbox.strategies.NetworkOnly({
        plugins: [bgSyncPlugin]
    }),
    'POST'
);

workbox.routing.registerRoute(
    // Cache CSS files.
    /\.css$/,
    // Use cache but update in the background.
    new workbox.strategies.StaleWhileRevalidate({
        // Use a custom cache name.
        cacheName: 'css-cache',
    })
);

//cache every route but POST : DEL / PUT
workbox.routing.registerRoute(
    // Cache CSS files.
   new RegExp('(?!.*\\?_method=)(?!.*\\.)(\\/.*)'),
    // Use cache but update in the background.
    new workbox.strategies.NetworkFirst({
        // Use a custom cache name.
        cacheName: 'html-cache',
    })
);

workbox.routing.registerRoute(
    /\.js$/,
    new workbox.strategies.StaleWhileRevalidate({
        // Use a custom cache name.
        cacheName: 'js-cache',
    })
);

workbox.routing.registerRoute(
    /\.json$/,
    new workbox.strategies.StaleWhileRevalidate({
        // Use a custom cache name.
        cacheName: 'json-cache',
    })
);

workbox.routing.registerRoute(
    // Cache image files.
    /\.(?:png|jpg|jpeg|svg|gif)$/,
    // Use the cache if it's available.
    new workbox.strategies.CacheFirst({
        // Use a custom cache name.
        cacheName: 'image-cache',
        plugins: [
            new workbox.expiration.Plugin({
                // Cache only 20 images.
                maxEntries: 20,
                // Cache for a maximum of a week.
                maxAgeSeconds: 7 * 24 * 60 * 60,
            })
        ],
    })
);

// This "catch" handler is triggered when any of the other routes fail to
// generate a response.
workbox.routing.setCatchHandler(({event}) => {
    // The FALLBACK_URL entries must be added to the cache ahead of time, either via runtime
    // or precaching.
    // If they are precached, then call workbox.precaching.getCacheKeyForURL(FALLBACK_URL)
    // to get the correct cache key to pass in to caches.match().
    //
    // Use event, request, and url to figure out how to respond.
    // One approach would be to use request.destination, see
    // https://medium.com/dev-channel/service-worker-caching-strategies-based-on-request-types-57411dd7652c
    switch (event.request.destination) {
        case 'document':
            return caches.match('/recipes/show');
            break;
        default:
            // If we don't have a fallback, just return an error response.
            return Response.error();
    }
});