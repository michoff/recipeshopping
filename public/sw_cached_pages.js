const cacheName = 'v1';

const cacheAssets = [
    '/',
    'about',
    'recipes/show',
    'recipes/add',
    'user/profile',
    'user/login',
    'user/register',
    'user/logout',
    'css/style.css',
    'css/bootstrap.min.css',
    'js/bootstrap.min.js',
    'js/bootstrap.js',
    'js/jquery.js',
    'js/main.js',
    'images/icon.png',
    'manifest.json',
    'favicon.ico',
    'js/indexedDB.js'
];

let recipe = {
    id: Number,
    persons: Number,
    name: String,
    instructions: String,
    personalNotes: String,
    ingredients: Array,
    userID: Number //should be changed with id
};


// Call Install Event
self.addEventListener('install', event => {
    console.log('Service Worker: Installed');
    event.waitUntil(
        caches.open(cacheName)
            .then(cache => {
                console.log('Service Worker: Caching Files');
                cache.addAll(cacheAssets);
            })
            .then(() => self.skipWaiting())
    );
});

// Call Activate Event
self.addEventListener('activate', e => {
    console.log('Service Worker: Activated');
    // Remove unwanted caches
    e.waitUntil(
        caches.keys()
            .then(cacheNames => {
                return Promise.all(
                    cacheNames.map(cache => {
                        if(cache !== cacheName) {
                            console.log('Service Worker: Clearing Old Cache');
                            return caches.delete(cache);
                        }
                    })
                )
            })
    )

});

// Call Fetch Event
self.addEventListener('fetch', e => {
    console.log('Service Worker: Fetching');
    e.respondWith(fetch(e.request).catch(() => caches.match(e.request)));
});
