const port = 80;
const express = require('express');
const path = require('path');
const exprHandle = require('express-handlebars');
const app = express();
const passport = require('passport');
const methodOverride = require('method-override');
const recipesPaths = require('./routes/recipes');
const userPaths = require('./routes/user');

// passport config
require('./config/passport')(passport);

/*
//ssl Keys
const https = require("https");
const fs = require("fs");

// Starting Secure Server
https.createServer({
    key: fs.readFileSync("./ssl/lyra.key"),
    cert: fs.readFileSync("./ssl/lyra.crt"),
}, app).listen(port, () => {
    console.log("[app.js] Server is running at port 80");
});
*/

// Localhost Debug variant
app.listen(8080, () => {
    console.log("it's running")
});

// Connect to mongoose
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/imgDatabase', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})
    .then(() => console.log('MongoDB connected...'))
    .catch(err => console.log(err));

// setting up static Folders
app.use('/js', express.static(path.join(__dirname + '/node_modules/localforage/dist')));
app.use('/js', express.static(path.join(__dirname + '/node_modules/bootstrap/dist/js/'))); // redirect bootstrap JS
app.use('/js', express.static(path.join(__dirname + '/node_modules/jquery/dist/'))); // redirect JS jQuery
app.use('/css', express.static(path.join(__dirname + '/node_modules/bootstrap/dist/css/'))); // redirect CSS bootstrap
app.use(express.static(path.join(__dirname, '/public')));

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({extended: true}));
// parse application/json
app.use(express.json());
// Adding method override after body parser
app.use(methodOverride('_method'));

// addinng krypting session
app.use(require('express-session')({
    secret: "secret stuff",
    resave: false,
    cookie: {
        maxAge: 7200000
    },
    saveUninitialized: false
}));
// passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Handlebar Middleware
app.engine('handlebars', exprHandle({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
// set up passport für user management

// making user accessible to handlebars
app.use(function (req, res, next) {
    res.locals.user = req.user;
    next();
});

// Use routes
app.use('/recipes', recipesPaths);
app.use('/user', userPaths);


// Get Index Route and print INDEX
app.get('/', (req, res, next) => {
    res.render('index', {
        title: 'RecipeBase'
    });
});

// About Page
app.get('/about', (req, res, next) => {
    res.render('about', {
        title: 'About'
    });
});

// redirect every other path to index
app.all(/^\/.*/, function (req, res) {
    res.redirect('/');
});